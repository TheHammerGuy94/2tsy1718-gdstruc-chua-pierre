#pragma once

#include <iostream>
#include <string>

#include "Item.h"

class ItemNode {
private: 
	Item item;
	int count = 0;
	ItemNode *pNext = NULL, *pPrev = NULL;
public:
	ItemNode();
	ItemNode(Item);
	~ItemNode();
	int getCount();
	void incCount();
	void decCount();
	void setItem(Item);
	Item getItem();
	ItemNode* getNext();
	ItemNode* getPrev();
	void setNext(ItemNode*);
	void setPrev(ItemNode*);
};

class Inventory
{
private:
	ItemNode * head = NULL, *activeItemNode = NULL;
	int size = 0;

	ItemNode* getNode(ItemNode* node, int index);
public:
	Inventory();
	~Inventory();
	void addItem(Item);
	Item at(int i);
	Item useSelectedItem();
	void itemUsed(bool success);
	void deleteNode(ItemNode*);
	
	void printItems();
	void getItemInput();
};

