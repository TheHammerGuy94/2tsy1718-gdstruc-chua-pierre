#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "Item.h"
#include "RNG.h"

class ItemGenerator
{
	private:
		vector<Item> itemList;
		int randInt(int);
		int randInt(int, int);
	public:
		ItemGenerator();
		~ItemGenerator();
		Item generateItem();
		void displayItems();
		int getListSize();
};

