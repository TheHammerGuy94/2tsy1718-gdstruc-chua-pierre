#include "ItemGenerator.h"



int ItemGenerator::randInt(int max){
	return RNG::getInstance()->randInt(max);
}

int ItemGenerator::randInt(int min, int max){
	return RNG::getInstance()->randInt(min,max);
}

ItemGenerator::ItemGenerator(){
	string line;
	string name;
	float value;
	int type, which;

	ifstream myfile;
	myfile.open("itemList.ite");

	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			//Item(string name, float value, ItemType type, int which = 0);
			istringstream linestream(line);
			linestream >> name >> value >> type >> which;
			this->itemList.push_back(Item(name, value, (ItemType)type, which));
		}
	}
}


ItemGenerator::~ItemGenerator(){}

Item ItemGenerator::generateItem(){
	return itemList[randInt(getListSize()-1)];
}

void ItemGenerator::displayItems(){
	cout << "items in this generator:" << endl;
	for (Item i : itemList) {
		cout << i.getName() << endl;
	}

}

int ItemGenerator::getListSize(){
	return this->itemList.size();
}
