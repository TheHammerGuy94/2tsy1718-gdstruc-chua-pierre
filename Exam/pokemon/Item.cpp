#include "Item.h"

bool Item::heal(Pokemon *pokemon){
	int value = 0;
	/*
		case 0: if curHP != 0 ||curHP != maxHP
			pokemon->heal
	*/
	if ((pokemon->getCurHP() == 0 && which == 0) || (pokemon->getCurHP()>0 && which == 1))
		return false;
	else {
		value = pokemon->heal(this->value);
		cout << "you healed " << pokemon->getName() << " by " << value << " hp " << endl;
		return true;
	}
}

bool Item::statBoost(Pokemon *pokemon){
	if (pokemon->modIsActive())
		return false;
	else {
		pokemon->statMod(this->value, this->which);
		cout << "you have boosted " << pokemon->getName() << "'s " << (which == 0 ? "attack" : "defense") << "for 1 battle"<<endl;
		return true;
	}
}

Item::Item(){}

Item::Item(string name, float value, ItemType type, int which){
	this->name = name;
	this->value = value;
	this->type = type;
	this->which = which;
}

Item::~Item(){}

string Item::getName(){
	return this->name;
}

void Item::setName(string name){
	this->name = name;
}

float Item::getValue(){
	return this->value;
}

void Item::setValue(float value){
	this->value = value;
}

int Item::getWhich(){
	return this->which;
}

void Item::setWhich(int which){
	this->which = which;
}

bool Item::useItem(Pokemon *poke){
	switch (this->type) {
	case ItemType::heal: 
		return heal(poke);

	case ItemType::booster:
		return statBoost(poke);
		default: return false;
	}
}
