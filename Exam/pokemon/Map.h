#pragma once

#include <iostream>
#include <string>

using namespace std;

enum Move {
	up = 1, down = 2, left = 3, right = 4, nowhere = 0
};
enum Tile {
	space = 0, wall = 1, grass = 2, item = 3, badge = 4
};

class Map
{
public:
	const int LENGTH = 100;
	const int MAXH = 26, MAXV = 49;
	Map();
	~Map();

	int getCurX();
	void setCurX(int);
	int getCurY();
	void setCurY(int);
	void movePosition(Move);
	void printMapWhole();
	void printMapScreen();
	void printMapScreen(int x, int y);
	Tile getCurrentTile();
	void printCurrentTile();
	string tileToString(Tile t);
	void decrementTiles();
	void disableTile(int length = -1);
	void disableTile(int i, int j, int length = -1);
	static char tileToChar(Tile);
	static Move inputToMove(char);

private:
	int curX = 1, curY = 1;
	Tile tileMap[50][27];
	int disableMap[50][27];
};

