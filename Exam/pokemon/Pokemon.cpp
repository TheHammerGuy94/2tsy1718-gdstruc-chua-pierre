#include "Pokemon.h"



Pokemon::Pokemon(){
	name = "";
}

Pokemon::Pokemon(string name, int maxHp, int att, int def,  int pow, int expBase){
	setStats(name, maxHp, att, def,  pow, expBase);
}

Pokemon::~Pokemon()
{}

void Pokemon::setStats(string name, int maxHp, int att, int def, int pow, int expBase){
	this->name = name;
	this->maxHp = maxHp;
	this->att = att;
	this->def = def;
	this->pow = pow;
	this->expBase = expBase;
	this->curHP = maxHp;
	this->expNow = 0;
}

string Pokemon::getName(){
	return this->name;
}

void Pokemon::setName(string name){
	this->name = name;
}

void Pokemon::setMaxHp(int maxHP) {
	this->maxHp= maxHP;
}

int Pokemon::getMaxHp() {
	return this->maxHp;
}

void Pokemon::setAtt(int att) {
	this-> att= att;
}

int Pokemon::getAtt() {
	if (which == 0)
		return floor(this->att * this->modifier);
	else
		return this->att;
}

void Pokemon::setDef(int def) {
	this-> def= def;
}

int Pokemon::getDef() {
	if (which == 1)
		return floor(this->def * this->modifier);
	return this->def;
}


void Pokemon::setPow(int pow) {
	this-> pow= pow;
}

int Pokemon::getPow() {
	return this->pow;
}

void Pokemon::setExpBase(int expBas) {
	this-> expBase= expBas;
}

int Pokemon::getExpBase() {
	return this->expBase;
}

int Pokemon::getExpGain(int level) {
	return this->getExpBase()*level*this->getAtt()/7;
}

void Pokemon::setCurHP(int hp) {
	this->curHP= hp;
	if (this->curHP > maxHp) this->curHP = maxHp;
	else if (this->curHP < 0) this->curHP = 0;
}

int Pokemon::getCurHP() {
	return this->curHP;
}

void Pokemon::setExpNow(int expNow) {
	this-> expNow= expNow;
}

int Pokemon::getExpNow() {
	return this->expNow;
}

void Pokemon::setLevel(int level) {
	this-> level= level;
}

int Pokemon::getLevel() {
	return this->level;
}

bool Pokemon::isPlayer(){
	return player;
}

void Pokemon::setPlayer(bool player){
	this->player = player;
}

bool Pokemon::isDefending(){
	return defending;
}

void Pokemon::setDefend(bool defend){
	this->defending = defend;
}

int Pokemon::takeDamage(int dmg){
	if (this->curHP < dmg) {
		dmg = curHP;
	}
	this->curHP -= dmg;
	return dmg;
}

int Pokemon::getActualDef(){
	return def * (defending)?2:1;
}

int Pokemon::getRequiredExp(){
	int exp = 1;
	for (int i = 1; i < level; i++) {
		exp += i;
	}
	return exp*100;
}

int Pokemon::getExpGain(Pokemon target){
	return target.getExpBase()*target.getLevel()*getAtt() / 7;
}

void Pokemon::insertExpGain(int exp){
	this->expNow += exp;
	if (expNow >= getRequiredExp()) {
		cout << "You leveled up!!!" << endl;
		setLevel(getLevel() + 1);
		gainStats();
	}

}

int Pokemon::attackTarget(Pokemon target){
	if (defending)
		return 0;
	return ((2 * this->level / 5 + 2)*this->pow * this->att / target.getActualDef()) / 50 + 2;
}

int Pokemon::heal(float hp){
	if (hp > 1.0f) {
		if (this->getCurHP() + hp > this->getMaxHp()) {
			hp = this->getMaxHp() - this->getCurHP();
		}
		this->curHP += hp;
	}
	else {
		if (this->getCurHP() == 0) {
			hp = this->getMaxHp()*hp;
		}
		this->curHP += hp;
	}
	return hp;
}

int Pokemon::healMax(){
	int hp = this->getMaxHp() - this->getCurHP();
	this->curHP = this->maxHp;

	return hp;
}


void Pokemon::gainStats(){
	this->maxHp += 5;
	att += RNG::getInstance()->randInt(2, 3);
	def += RNG::getInstance()->randInt(2, 3);
}

void Pokemon::gainStats(int targetLevel){
	for (int i = this->getLevel(); i < targetLevel; i++) {
		this->gainStats();
	}
	this->setLevel(targetLevel);
}

void Pokemon::statMod(float value, int which){
	this->modifier = value;
	this->which = which;
}

void Pokemon::resetMod(){
	this->modifier = 1.0f;
	this->which = 0;
}

bool Pokemon::modIsActive(){
	return modifier != 1.0f;
}

void Pokemon::displayHealthBar(){
	int length = 20;
	int bar = floor(curHP /maxHp)*length;

	cout << "[";
	for (int i = 1; i <= length; i++) {
		if (bar <= i)
			cout << "=";
		else
			cout << " ";
	}
	cout << "]" <<curHP<<"/"<<maxHp<< endl;
}

void Pokemon::displayStatsBattle(){
	// pokemon:
	//[=====] x/y
	cout << this->getName() << ":" << endl;
	displayHealthBar();
}


Pokemon * Pokemon::generatePokemon(){
	Pokemon *generate = new Pokemon(name,maxHp,att,def,pow,expBase);
	return generate;
}

