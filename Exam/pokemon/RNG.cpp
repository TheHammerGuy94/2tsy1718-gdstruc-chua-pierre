#include "RNG.h"

RNG *RNG::main = NULL;

RNG::RNG(){
	rd = default_random_engine(time(0));
	randFl = uniform_real_distribution<double>(0.0f, 1.0f);
}

RNG::~RNG(){
	if (main != NULL)
		delete main;
}

RNG* RNG::getInstance(){
	if (main == NULL)
		main = new RNG();
	return main;
}
double RNG::randFloat(){
	return randFl(rd);
}

double RNG::randFloat(double mult){
	return randFloat()*mult;
}

int RNG::randInt(int max){
	return randFloat(max);
}

int RNG::randInt(int min, int max){
	if (max == min)
		return min;
	if (min > max) {
		int temp = max;
		max = min;
		min = max;
	}
	return randInt(max+1 - min) + min;
}
