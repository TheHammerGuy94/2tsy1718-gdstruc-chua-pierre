#include <iostream>
#include <string>

#include "Map.h"
#include "Pokemon.h"
#include "PokGenerator.h"
#include "Item.h"
#include "Inventory.h"
#include "ItemGenerator.h"
#include "RNG.h"

using namespace std;

struct Trainer {
	string name;
	Pokemon pokemon;
	Inventory bag;
	int badges = 0;
};

void itemUI(Trainer *t) {
	int input = 0;
	Item item;
	bool used = false;

	do {
		cout << "your inventory" << endl;
		t->bag.printItems();
		cout << "which one would you like to use? (press 0 to go back to the game)" << endl << ">>>";
		cin >>input;
		if (input != 0) {
			item = t->bag.at(input - 1);
			used = item.useItem(&t->pokemon);
			if (!used)
				cout << "There's a time and place for everything" << endl<<"BUT NOT NOW!!!";
			t->bag.itemUsed(used);
		}
		system("cls");
	} while (input == 0);
}

bool fiftyFiftyRng() {
	return RNG::getInstance()->randFloat() < 0.5f;
}

void encounterPokemon(Trainer *t) {
	static PokGenerator gen;
	Pokemon pok = t->pokemon;
	Pokemon *wil = gen.generateEncounter(pok.getLevel());
	bool run = false;
	string input = "";
	int wilDmg = 0, pokDmg =0;

	system("cls");
	cout << "A wild " << wil->getName() << " appeared!!!" << endl;
	do {
		cout << "STATS==========" << endl;
		pok.displayStatsBattle();
		wil->displayStatsBattle();
		cout << "your Move:" << endl;
		cout << "[a] to attack" << endl;
		cout << "[d] to defend" << endl;
		cout << "[r] to run" << endl;
		getline(cin, input);

		pok.setDef(tolower(input[0]) == 'd');
		wil->setDef(fiftyFiftyRng());

		if (tolower(input[0]) == 'r'){
			run = fiftyFiftyRng();
			if (run) 
				cout << "got away safely!!!" << endl;
			else 
				cout << "can't escape!!!" << endl;
		}
		else {
			wilDmg = pok.attackTarget(*wil);
		}
		pokDmg = wil->attackTarget(pok);
		if (!run) {
			if(wilDmg > 0) cout << wil->getName() << " took " << wilDmg << " damage!!!" << endl;
			cout << pok.getName() << " took " << pokDmg << " damage!!!" << endl;

			wil->takeDamage(wilDmg);
			pok.takeDamage(wilDmg);
		}
		system("pause");
		system("cls");
	} while (pok.getCurHP() > 0 && wil->getCurHP() > 0 &&!run);
	if (!run && pok.getCurHP() > 0) {
		int exp = wil->getExpGain(pok.getLevel());
		cout << pok.getName() << " gained " << exp << " points!!!" << endl;
	}

	delete wil;

	t->pokemon = pok;
}



void spawnItem(Trainer *t) {
	static ItemGenerator gen;
	Item item = gen.generateItem();
	cout << "you found a " << item.getName();
	t->bag.addItem(item);
}

void executeMove(Trainer *t, Map *map) {
	Tile tile = map->getCurrentTile();
	if (t->pokemon.getCurHP() == 0 && tile != Tile::item)
		tile = Tile::space;

	switch (tile) {
		case Tile::grass:
			if (RNG::getInstance()->randFloat() < 0.2f) {
				encounterPokemon(t);
			}
			else
				cout << "No Pokemon Appeared" << endl;
			system("pause");
			break;
		case Tile::badge:
			cout << "you found a badge!!!" << endl;
			map->disableTile(-1);
			system("pause");
			break;
		case Tile::item:
			spawnItem(t);
			map->disableTile(100);
			system("pause");
			break;
	}
	map->decrementTiles();
	
}

int main() {
	Trainer trainer;
	PokGenerator gen;
	Map map;
	int temp;
	char input;
	Move move = nowhere;

	cout << "what is your name?" << endl << ">>>";
	cin >> trainer.name;
	cout << "Select your starter:" << endl;
	gen.displayStarters();
	cin >> temp;
	trainer.pokemon = *gen.selectStarter(temp - 1);

	cout << "let's Begin:" << endl;
	system("pause");

	while (trainer.badges < 4) {
		map.printMapScreen();
		cout << "===========" << endl;
		map.printCurrentTile();
		cout << "badges: " << trainer.badges << "/" << 4 << endl;
		trainer.pokemon.displayStatsBattle();
		cout << "what would you like to do?" << endl;
		cout << "[w/s/a/d] for movement" << endl;
		cout << "[i] to view inventory" << endl;
		cin >> input;
		move = Map::inputToMove(input);

		if (move != Move::nowhere) {
			map.movePosition(move);
			executeMove(&trainer, &map);
		}
		else if(input == 'i'){
			itemUI(&trainer);
		}
		system("cls");
	}

	system("pause");
	return 0;
}