#include "Map.h"

Map::Map(){
	int map[50][27] = {
		{ 1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,1,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,1,1,1,1,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,0,1,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,0,1,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,4,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,1,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,1,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,3,2,2,2,2,2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,2,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,2,0,1,1,3,2,2,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,2,0,1,1,0,2,2,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 1,2,2,2,2,2,2,2,4,0,1,1,1,2,2,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 1,0,0,1,1,1,1,1,1,1,1,0,1,2,2,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 1,1,0,1,0,0,0,0,0,0,0,0,1,0,0,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 1,1,0,1,1,1,1,1,1,1,1,1,1,0,0,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,2,2,1,0,0,0,0,0,0,0 },
		{ 0,1,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0 },
		{ 0,1,0,1,0,0,0,0,1,0,0,1,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1 },
		{ 1,1,0,1,0,0,0,0,1,0,0,1,2,2,2,2,2,2,2,1,0,1,2,2,2,4,1 },
		{ 1,0,0,1,1,1,1,0,1,0,0,1,2,2,2,2,2,2,2,1,0,1,2,2,2,2,1 },
		{ 1,2,2,2,2,0,1,0,1,0,0,1,1,2,2,2,2,2,2,1,0,1,2,2,2,2,1 },
		{ 1,2,2,2,2,0,1,1,1,2,2,2,1,2,2,2,2,2,0,1,0,1,2,2,2,2,1 },
		{ 1,2,2,2,2,0,1,1,2,2,2,2,1,1,1,1,1,1,1,1,1,1,2,2,2,2,1 },
		{ 1,0,0,0,0,0,1,1,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1 },
		{ 1,0,0,0,0,4,1,1,3,2,2,2,0,0,1,1,1,1,1,1,1,1,1,1,0,0,1 },
		{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 }
	};

	for (int i = 0; i < 50; i++) {
		for (int j = 0; j < 27; j++) {
			tileMap[i][j] = (Tile)map[i][j];
			disableMap[i][j] = 0;
		}
	}

}


Map::~Map()
{}

int Map::getCurX()
{
	return this->curX;
}

void Map::setCurX(int x){
	this->curX = x;
}

int Map::getCurY()
{
	return this->curY;
}

void Map::setCurY(int y){
	this->curY = y;
}

void Map::movePosition(Move move){
	switch (move) {
		case Move::up:
			if (tileMap[curY - 1][curX] != Tile::wall)
			curY--;
			break;
		case Move::down:
			if (tileMap[curY + 1][curX] != Tile::wall)
			curY++;
			break;
		case Move::left:
			if (tileMap[curY][curX - 1] != Tile::wall)
			curX--;
			break;
		case Move::right:
			if (tileMap[curY][curX + 1] != Tile::wall)
			curX++;
			break;
	}
}

void Map::printMapWhole(){
	for (int i = 0; i < 50; i++) {
		for (int j = 0; j < 27; j++) {
			cout << tileToChar(tileMap[i][j]);
		}
		cout << endl;
	}
}

void Map::printMapScreen(){
	printMapScreen(curX, curY);
}

void Map::printMapScreen(int x, int y){
	int minY, minX;
	int clampX, clampY;
	minY = y-5; clampY = y+5;
	minX = x-5; clampX = x+5;


	if (minY< 0) {
		clampY -= minY;
		minY -= minY;
	}
	else if (clampY > MAXV) {
		minY -= MAXV - clampY;
		clampY -= MAXV - clampY;
	}
	if (minX < 0) {
		clampX -= minX;
		minX -= minX;
	}
	else if (clampX > MAXH) {
		minX -= MAXH - clampX;
		clampX -= MAXH - clampX;
	}

	for (int i=minY; i <= clampY; i++) {
		for (int j = minX; j <= clampX ; j++) {
			if (i == curY && j == curX)
				cout << 'P';
			else if (disableMap[i][j] != 0)
				cout << tileToChar(Tile::space);
			else
				cout << tileToChar(tileMap[i][j]);
		}
		cout<<endl;
	}
}

Tile Map::getCurrentTile(){
	if (disableMap[curY][curX] == 0)
		return tileMap[curY][curX];
	else
		return Tile::space;
}

void Map::printCurrentTile(){
	cout << "you are on: " << tileToString(getCurrentTile()) << endl;
}

string Map::tileToString(Tile t){
	switch (t) {
		case wall: return "Wall";
		case grass: return "Grass";
		case item: return "Item";
		case badge: return "Badge";
		default: return "";
	}
}

void Map::decrementTiles(){
	for (int i = 0; i <= MAXV; i++) {
		for (int j = 0; j <= MAXH; j++) {
			if (disableMap[i][j] > 0) disableMap[i][j]--;
		}
	}
}

void Map::disableTile(int length){
	disableTile(curY, curX, length);
}

void Map::disableTile(int i, int j, int length){
	disableMap[i][j] = length;
}

char Map::tileToChar(Tile tile){
	switch (tile) {
		case wall: return 'X';
		case grass: return 'w';
		case item: return 'I';
		case badge: return 'B';
		default: return ' ';
	}
}

Move Map::inputToMove(char c){
	Move moveInput = nowhere;
	
	switch (c) {
		case 'w':
			moveInput = Move::up;
			break;
		case 'a':
			moveInput = Move::left;
			break;
		case 's':
			moveInput = Move::down;
			break;
		case 'd':
			moveInput = Move::right;
			break;
	}
	return moveInput;
}
	
