#pragma once

#include <iostream>
#include <string>

#include "PokGenerator.h"

using namespace std;

enum ItemType {
	heal = 0, booster = 1
};

class Item
{
private:
	string name;
	float value;
	int which;
	enum ItemType type;

	bool heal(Pokemon *);
	bool statBoost(Pokemon *);

public:
	Item();
	Item(string name, float value, ItemType type, int which = 0);
	~Item();

	//place getters and setters here
	string getName();
	void setName(string);
	float getValue();
	void setValue(float);
	int getWhich();
	void setWhich(int);
	bool useItem(Pokemon *);

};

