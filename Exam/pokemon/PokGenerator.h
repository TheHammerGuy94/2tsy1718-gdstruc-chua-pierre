#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "Pokemon.h"
#include "RNG.h"

using namespace std;


class PokGenerator
{
private:
	vector<Pokemon> pokemonList,starterList;
	int randInt(int);
	int randInt(int, int);
public:
	PokGenerator();
	~PokGenerator();

	Pokemon generateEncounter();
	Pokemon* generateEncounter(int playerLevel);
	void displayPokemon();
	void displayStarters();
	Pokemon* selectStarter(int);
	int getEncounterSize();


};

