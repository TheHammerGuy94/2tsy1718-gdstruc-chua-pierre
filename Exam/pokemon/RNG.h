#pragma once
#include <random>
#include <ctime>

using namespace std;

class RNG
{

private:
	static RNG *main;
	default_random_engine rd;
	uniform_real_distribution<double> randFl;
	RNG();

public:
	~RNG();
		//singleton pattern. omnipresent in the program.
	static RNG* getInstance();
		//generates number from 0 - 1 float exclusive
	double randFloat();
		//generates a number from 0 - input exclusive
	double randFloat(double);
		//generates 0 - input
	int randInt(int);
		//RNG min-max, will error correct if needed
	int randInt(int min,int max);

};

