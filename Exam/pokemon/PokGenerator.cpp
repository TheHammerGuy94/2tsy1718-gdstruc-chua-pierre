#include "PokGenerator.h"

int PokGenerator::randInt(int max){
	return RNG::getInstance()->randInt(max);
}

int PokGenerator::randInt(int min, int max){
	return RNG::getInstance()->randInt(min, max);
}

PokGenerator::PokGenerator(){
	string line;
	string name;
	int hp, attack, defense, power,basExp;
	ifstream myfile;
	myfile.open("pokemonStats.poke");

	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			istringstream linestream(line);
			linestream >> name >>hp >>attack >> defense >> power>>basExp;
			this->pokemonList.push_back(Pokemon(name, hp, attack, defense, power, basExp));
		}
		starterList.push_back(this->pokemonList[0]);
		starterList.push_back(this->pokemonList[1]);
		starterList.push_back(this->pokemonList[2]);
		pokemonList.erase(pokemonList.begin() + 0);
		pokemonList.erase(pokemonList.begin() + 0);
		pokemonList.erase(pokemonList.begin() + 0);
	}
	myfile.close();
}
PokGenerator::~PokGenerator(){}

Pokemon PokGenerator::generateEncounter(){
	return pokemonList[randInt(pokemonList.size() - 1)];
}

Pokemon * PokGenerator::generateEncounter(int playerLevel){
	Pokemon *generate = generateEncounter().generatePokemon();
	playerLevel = randInt(playerLevel - 5, playerLevel + 5);
	if (playerLevel < 1) playerLevel = 1;

	generate->gainStats(playerLevel);
	generate->setPlayer(false);

	return generate;
}

void PokGenerator::displayPokemon(){
	for (Pokemon p : pokemonList) {
		cout << p.getName() << endl;
	}
}

void PokGenerator::displayStarters(){
	for (Pokemon p : starterList) {
		cout << p.getName()<<endl;
	}
}

Pokemon * PokGenerator::selectStarter(int i){
	return starterList[i].generatePokemon();
}

int PokGenerator::getEncounterSize(){
	return pokemonList.size();
}
