#pragma once

#include<iostream>
#include<string>
#include<cmath>

#include "RNG.h"

using namespace std;

class Pokemon
{
private:
	string name;
	int maxHp,att, def, pow, expBase;
	int curHP, expNow;
	int level = 1;
	float modifier = 1.0;
	int which = 0;
	bool defending = false, player = false;
public:
	Pokemon();
	Pokemon(string name, int maxHp, int att, int def,  int pow, int expBase);
	~Pokemon();
	void setStats(string name, int maxHp, int att, int def,  int pow, int expBase);
	
	string getName();
	void setName(string);
	void setMaxHp(int); int getMaxHp();
	void setAtt(int);	int getAtt();
	void setDef(int);	int getDef();
	void setPow(int);	int getPow();
	void setExpBase(int);int getExpBase();
	int getExpGain(int);
	void setCurHP(int);	int getCurHP();
	void setExpNow(int);int getExpNow();
	void setLevel(int);	int getLevel();
	bool isPlayer();	void setPlayer(bool);
	bool isDefending(); void setDefend(bool);

	int takeDamage(int);
	int getActualDef();
	int getRequiredExp();
	int getExpGain(Pokemon);
	void insertExpGain(int);
	int attackTarget(Pokemon);
	int heal(float);
	int healMax();
	void gainStats();
	void gainStats(int targetLevel);
	void statMod(float, int);
	void resetMod();
	bool modIsActive();
	void displayHealthBar();
	void displayStatsBattle();
	Pokemon* generatePokemon();
};

