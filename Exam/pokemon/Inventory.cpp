#include "Inventory.h"

ItemNode* Inventory::getNode(ItemNode * node, int index){
	if (index == 0)
		return node;
	else
		return getNode(node->getNext(), index - 1);

}

Inventory::Inventory(){}


Inventory::~Inventory(){
	//put the deleteEverythhing funnction here
}

void Inventory::addItem(Item item){
	if (this->size == 0) {
		head = new ItemNode(item);
	}
	else {
		//search for the item
		ItemNode *run = head; 
		int found = -1;
		for (int i = 0; i < size; i++) {
			if (run->getItem().getName() == item.getName()) {
				found = i; 
				i = size;
			}
			else
				run = run->getNext();
		}
		if (found == -1) {
			run = new ItemNode(item);
			run->setPrev(head->getPrev());
			run->getPrev()->setNext(run);
			run->setNext(head);
			run->getNext()->setPrev(run);
		}
		else {
			run->incCount();
		}
	}
}

Item Inventory::at(int i){
	if (i == 0)
		activeItemNode = head;
	else
		activeItemNode = getNode(head->getNext(), i - 1);

	return activeItemNode->getItem();
}

Item Inventory::useSelectedItem(){
	return activeItemNode->getItem();
}

void Inventory::itemUsed(bool success){
	if (success) {
		activeItemNode->decCount();
		if (activeItemNode->getCount() == 0)
			deleteNode(activeItemNode);
	}
}

void Inventory::deleteNode(ItemNode *node){

	if(size != 1){
		ItemNode *next = node->getNext();
		ItemNode *prev = node->getPrev();
		if (node == head) head = next;
		next->setPrev(prev);
		prev->setNext(next);
	}
	delete node;
	this->size--;
}

void Inventory::printItems(){
	ItemNode *run = head;
	for (int i = 0; i < this->size; i++) {
		cout << "[" << (i + 1) << "] (x" << head->getCount()<<") "<<head->getItem().getName() << endl;
		run = run->getNext();
	}
}

void Inventory::getItemInput(){
	int in;
	printItems();
	cout << "[0] to exit Inventory" << endl;
	cout << "which Item would you like to use?\n>>>";
	cin >> in;
	
	if (in != 0) {
		in--;
		at((in - 1));
	}
}

ItemNode::ItemNode(){}

ItemNode::ItemNode(Item i) {
	this->item = i;
}

ItemNode::~ItemNode(){}

int ItemNode::getCount(){
	return this->count;
}

void ItemNode::incCount(){
	this->count++;
}

void ItemNode::decCount(){
	this->count--;
}

void ItemNode::setItem(Item i){
	this->item = i;
}

Item ItemNode::getItem(){
	return item;
}

ItemNode* ItemNode::getNext(){
	return this->pNext;
}

ItemNode* ItemNode::getPrev(){
	return this->pPrev;
}

void ItemNode::setNext(ItemNode *iNode){
	this->pNext = iNode;
}

void ItemNode::setPrev(ItemNode *iNode){
	this->pPrev = iNode;
}
