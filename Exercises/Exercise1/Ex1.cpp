#include <iostream>
#include <string>

using namespace std;

/*
Ex 1 - 1
Create a variable for a Character
Name
Level STR STA SPR
*/

//exercise 1-1
struct Player 
{
	string name;
	int lvl, str, sta, spr;
};

int getHp(Player play);
int getMp(Player play);
int getAtk(Player play);
void ioData();

string stringInput(string msg) 
{
	bool error = false;
	string temp = "";
	do 
	{
		error = false;
		cout << msg << endl;
		cin >> temp;

		if (temp.length() < 1 || cin.fail())
		{
			cout << "INPUT SOMETHING!!!" << endl;
			system("pause");
			cin.clear();
			cin.ignore('\n', 20);
			error = true;
		}
	} while (error);
	
	return temp;
}

int intInput(string msg, bool acceptZero = true) 
{
	bool error = false;
	int temp = 0;
	do
	{
		error = false;
		cout << msg << endl;
		cin >> temp;

		if (cin.fail() )
		{
			cout << "INPUT Error!!!" << endl;
			cin.clear();
			cin.ignore('\n', 20);
			error = true;
		}
		if ((temp == 0 && !acceptZero) || temp < 0)
		{
			cout << "NO NEGATIVE NUMBERS!!!" << endl;
			if (!acceptZero && temp == 0)
				cout << "CANNOT BE ZERO!!!" << endl;
			error = true;
		}
		if (error)
		{
			system("pause");
			system("cls");
		}

	} while (error);

	return temp;
}

int main() 
{
	ioData();
	system("pause");
	return 0;
}

//exercise 1-2 getHP
int getHp(Player play) 
{
	int factor = 100;
	return (play.lvl * factor) + (play.lvl * factor)*0.01f*play.sta;
}

//exercise 1-3 getMp
int getMp(Player play) 
{
	int factor = 10;
	return (play.lvl * factor) + (play.lvl * factor)*(0.01f*play.spr);
}

//exercise 1-4 getAttack
int getAtk(Player play) 
{
	return (play.lvl * 2) + (play.str);
}

// exercise 1-5 input and output everything
void ioData() 
{
	Player play;
	bool error = false;

	cout << "what's your name?";
	cin >> play.name;
	
	cout << "input the following data:" << endl;

	play.lvl = intInput("Level: ", false);
	play.str = intInput("Strength: ");
	play.sta = intInput("Stamina: ");
	play.spr = intInput("Spirit: ");

	system("cls");
	cout << "Name:\t" << play.name << endl
		<< "Atk:\t" << getAtk(play) << endl
		<< "HP:\t" << getHp(play) << endl
		<< "MP:\t" << getMp(play) << endl<<endl;

}