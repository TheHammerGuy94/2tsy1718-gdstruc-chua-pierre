#include <iostream>
#include <string>
#include <random>
#include <ctime>
#include <cmath>

//Code by Pierre Chua
//and Jaynel Dugos

using namespace std;

const int HEAD = 0, TORSO = 1, LEGS = 2, ARMS = 3, FEET = 4;
const int STR = 0, AGI = 1, INT = 2, VIT = 3;

enum dmgType
{
	Physical,
	Magic
};

enum classType
{
	Warrior,
	Archer,
	Sorceress,
	Cleric,
	Academic,
	Kali,
	Assassin,
	Lancea,
	Machina
};

struct CharStat
{
	int STR;
	int AGI;
	int INT;
	int VIT;
	void printStats();
};

void CharStat::printStats() {
	cout << "\tSTR: " << CharStat::STR << endl;
	cout << "\tAGI: " << CharStat::AGI << endl;
	cout << "\tINT: " << CharStat::INT << endl;
	cout << "\tVIT: " << CharStat::VIT << endl;
}

struct CharStatCalc
{
	float STR;
	float AGI;
	float INT;
	float VIT;
};

struct Weapon
{
	int physDmgMin;
	int physDmgMax;
	int magDmgMin;
	int magDmgMax;
	string name;
	bool isPrimary = false;

	int getPhysDmg(); //Post-Defined
	int getMagDmg(); //Post-defined
	void printWeapon();
};
void Weapon::printWeapon() {
	
}


struct Armor
{
	string name;
	int physDef;
	int magDef;
	int armorPart;
	int durability;
	void printArmor();
};

struct Accessory
{
	string name;
	int stat;
	float gain = 1.0f;
	void printAcc();
};

struct Character
{
	classType type;

	string name;
	int level;
	int hpMax;
	int hpCur;
	int mpMax;
	int mpCur;
	string guild;

	Weapon weapon0, weapon1;

	Armor armor[5];
	Accessory accs[4];

	CharStat baseStat;
	CharStatCalc statCalc;

	void applyBuffs();

	float getClassPhysDmgBonus();
	float getClassMagDmgBonus();
	float getClassPhysDefBonus();

	int getDmg(float statBonus, int weapDmg);

	int getPhysDmgMin();
	int getPhysDmgMax();
	int getMagDmgMin();
	int getMagDmgMax();

	int getDef(Armor armor[], float bonus, dmgType type);
	int getPhysDef();
	int getMagDef();
	void printChararcter();
};

//Weapons 
int Weapon::getPhysDmg() {
	return rand() % (physDmgMax + 1 - physDmgMin) + physDmgMin;
}

int Weapon::getMagDmg() {
	return rand() % (magDmgMax + 1 - magDmgMin) + magDmgMin;
}

//Character
void Character::applyBuffs()
{
	float *proxStatCalc = 0;
	int *proxStat = 0;

	for (int i = 0; i< 5; i++)
	{
		if (Character::accs[i].name != "") {
			switch (Character::accs[i].stat)
			{
			case 0: proxStatCalc = &(Character::statCalc.STR);
				proxStat = &(Character::baseStat.STR);
				break;
			case 1: proxStatCalc = &(Character::statCalc.AGI);
				proxStat = &(Character::baseStat.AGI);
				break;
			case 2: proxStatCalc = &(Character::statCalc.INT);
				proxStat = &(Character::baseStat.INT);
				break;
			case 3: proxStatCalc = &(Character::statCalc.VIT);
				proxStat = &(Character::baseStat.VIT);
				break;
			}
			*proxStatCalc = *proxStat + Character::accs[i].gain;
		}
	}
}

float Character::getClassPhysDmgBonus()
{
	float strDmg = 0.0f;
	float agiDmg = 0.0f;

	//Strength Bonus
	switch (type)
	{
	case Assassin:
		strDmg = 0.75f;
		break;

	case Archer:
		strDmg = 0.25f;
		break;

	case Sorceress:
		strDmg = 0.15f;
		break;
	}

	//Agility bonus
	switch (type)
	{
	case Machina:
		agiDmg = 0.75f;
		break;

	case Assassin:
		agiDmg = 0.5f;
		break;

	case Sorceress:
		agiDmg = 0.3f;
		break;

	case Warrior:
	case Cleric:
	case Kali:
	case Lancea:
		agiDmg = 0.25f;
		break;
	}

	return statCalc.STR * strDmg + statCalc.AGI * agiDmg;
}

float Character::getClassMagDmgBonus()
{
	float intDmg = 0.0f;

	//Intelligence bonus
	switch (type)
	{
	case Sorceress:
		intDmg = 0.8f;
		break;

	case Machina:
		intDmg = 0.25f;
		break;
	}

	return statCalc.INT * intDmg;
}

float Character::getClassPhysDefBonus()
{
	float vitDef = 0.0f;

	switch (type)
	{
	case Sorceress:
	case Academic:

		vitDef = 0.72f;
		break;

	case Warrior:
	case Archer:
	case Kali:
	case Assassin:
	case Cleric:
	case Lancea:
	case Machina:

		vitDef = 0.6f;
		break;
	}

	return statCalc.VIT * vitDef;
}

int Character::getDmg(float statBonus, int weapDmg)
{
	float dmg = statBonus + weapDmg;
	return round(dmg);
}

int Character::getPhysDmgMin() {
	return Character::getDmg(Character::getClassPhysDmgBonus(), Character::weapon0.physDmgMin + Character::weapon1.physDmgMin);
}
int Character::getPhysDmgMax() {
	return Character::getDmg(Character::getClassPhysDmgBonus(), Character::weapon0.physDmgMax + Character::weapon1.physDmgMax);
}
int Character::getMagDmgMin() {
	return Character::getDmg(Character::getClassMagDmgBonus(), Character::weapon0.magDmgMin + Character::weapon1.magDmgMin);
}
int Character::getMagDmgMax() {
	return Character::getDmg(Character::getClassMagDmgBonus(), Character::weapon0.magDmgMax + Character::weapon1.magDmgMax);
}

int Character::getDef(Armor armor[], float bonus, dmgType type)
{
	for (int i = 0; i < 5; i++)
	{
		switch (type)
		{
		case Physical:
			bonus += armor[i].physDef;
			break;
		case Magic:
			bonus += armor[i].magDef;
			break;
		}
	}
	return round(bonus);
}

int Character::getPhysDef()
{
	return getDef(Character::armor, Character::getClassPhysDefBonus(), Physical);
}

int Character::getMagDef() {
	return getDef(Character::armor, 0.0f, Magic);
}

int main()
{
	Character showCharStats;
	CharStat *showStats = &(showCharStats.baseStat);
	Weapon *weap0, *weap1;
	Armor *showArm;
	Accessory *showAcc;

	weap0 = &(showCharStats.weapon0);
	weap1 = &(showCharStats.weapon1);

	showArm = showCharStats.armor;
	showAcc = showCharStats.accs;

	showCharStats.name = "Zeineru";
	showCharStats.level = 15;
	showCharStats.hpMax = 3600;
	showCharStats.hpCur = 3600;
	showCharStats.mpMax = 2500;
	showCharStats.mpCur = 2500;
	showCharStats.guild = "Order of the Blue Lightning";

	// HP Computation = 200 HP per Level + 30 HP per VIT
	// SP Computation = 100 SP per Level + 40 SP per INT
	// Note: This is an improvised computation I made.

	//Primary and Secondary Weapon

	weap0->name = "Iron Wand of Power";
	weap0->physDmgMin = 27;
	weap0->physDmgMax = 41;
	weap0->magDmgMin = 44;
	weap0->magDmgMax = 66;
	weap0->isPrimary = true;

	weap1->name = "Iron Shield of the Tabernacle";
	weap1->physDmgMin = 23;
	weap1->physDmgMax = 34;
	weap1->magDmgMin = 23;
	weap1->magDmgMax = 34;
	weap1->isPrimary = false;

	//Armor:

	const int HEAD = 0, TORSO = 1, LEGS = 2, ARMS = 3, FEET = 4;

	showArm[0].name = "Iron Galero";
	showArm[0].physDef = 221;
	showArm[0].magDef = 197;
	showArm[0].armorPart = 0;
	showArm[0].durability = 120;

	showArm[1].name = "Iron Armor";
	showArm[1].physDef = 277;
	showArm[1].magDef = 146;
	showArm[1].armorPart = 1;
	showArm[1].durability = 120;

	showArm[2].name = "Iron Trousers";
	showArm[2].physDef = 277;
	showArm[2].magDef = 146;
	showArm[2].armorPart = 2;
	showArm[2].durability = 120;

	showArm[3].name = "Iron Mitts";
	showArm[3].physDef = 110;
	showArm[3].magDef = 98;
	showArm[3].armorPart = 3;
	showArm[3].durability = 120;

	showArm[4].name = "Brass Shoes";
	showArm[4].physDef = 71;
	showArm[4].magDef = 63;
	showArm[4].armorPart = 4;
	showArm[4].durability = 120;


	//Accessories
	showAcc[0].name = "Grace Earrings";
	showAcc[0].stat = INT;
	showAcc[0].gain = 3.0f;

	showAcc[1].name = "Grace Necklace";
	showAcc[1].stat = INT;
	showAcc[1].gain = 3.0f;

	showAcc[2].name = "Sage's Cubic Ring";
	showAcc[2].stat = AGI;
	showAcc[2].gain = 6.0f;

	showAcc[3].name = "Stunning Midnight Dark Ring";
	showAcc[3].stat = VIT;
	showAcc[3].gain = 4.0f;

	// These values are whole numbers added to the stat. (e.g Grace Earrings +3 INT, 65 + 3 = 68)

	showStats->STR = 10;
	showStats->AGI = 10;
	showStats->INT = 10;
	showStats->VIT = 10;

	//pierre: I'm here
	/*
	so at this point we can proceed to printing
	so, you go on ahead putting in some values imma code some "print this struct var"
	for now, the stuff above the int main() fuunction I am currently editing that in visual studio
	but legit, im tired AF right now...
	*/

	cout << "~~~~~~~~~~~~~~Character~~~~~~~~~~~~~~" << endl << endl;

	/*
	Print the ff:

	Name

	Class
	Level
	HP
	MP
	Guild

	Strength
	Agility
	Intellect
	Vitality

	Physical Damage (Min to Max)
	Magical Damage (Min to Max)
	Defense
	Magic Defense

	Primary Weapon
	Secondary Weapon

	Head
	Torso
	Legs
	Arms
	Feet
	Accessories (1 to 4)
	*/

	system("pause");
	return 0;
}