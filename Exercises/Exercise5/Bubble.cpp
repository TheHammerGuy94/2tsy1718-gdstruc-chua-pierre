#include <iostream>
#include <string>
using namespace std;

void swap(int arr[], int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

void bubbleSort(int arr[], int size) {
	for (int i = size-2; i > 1; i--) {
		for (int j = 0; j <= i; j++) {
			if (arr[j] > arr[j + 1]) swap(arr, j, j + 1);
		}
	}
}



int main(){
	int nums[10] = { 9,10,2,3,50,99,50,20,35,40 };
	int numsSize = sizeof(nums) / sizeof(nums[0]);

	cout << "numbers in list:" << endl;

	for (int i : nums) {
		cout << "|" << i;
	}
	cout << "|" << endl << endl;
	
	bubbleSort(nums, numsSize);
	
	for (int i : nums) {
		cout << "|" << i;
	}
	cout << "|" << endl<<"...";
	system("pause>null");
	return 0;
}

