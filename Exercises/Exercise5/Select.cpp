#include <iostream>
#include <string>
using namespace std;

void swap(int arr[], int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

void selectionSort(int arr[], int size) {

	for (int i = 0; i < size; i++){
		int k = i;
		for (int j = i; j < size; j++) {
			if (arr[j] < arr[k])
				k = j;
		}
		swap(arr, i, k);

	}
}



int main() {
	int nums[10] = { 9,10,2,3,50,99,50,20,35,40 };
	int numsSize = sizeof(nums) / sizeof(nums[0]);

	cout << "numbers in list:" << endl;

	for (int i : nums) {
		cout << "|" << i;
	}
	cout << "|" << endl << endl;

	selectionSort(nums, numsSize);

	for (int i : nums) {
		cout << "|" << i;
	}
	cout << "|" << endl << "...";
	system("pause>null");
	return 0;
}

