#include <iostream>

using namespace std;

struct Node {
	int num;
	Node *pNext;
};

struct LList {
	unsigned int size;
	Node *pFirst, *pLast;
};

void addFront(LList *list, int i) {
	Node *temp = new Node;
	list->size++;
	if (list->size == 0) {
		list->pFirst = temp;
		list->pLast = list->pFirst;
	}
	else {
		temp->pNext = list->pFirst;
		list->pFirst = temp;
	}
}

void addLast(LList *list, int i) {
	list->size++;
	Node *temp = new Node;
	if (list->size == 0) {
		list->pFirst = temp;
		list->pLast = list->pFirst;
	}
	else {
		list->pLast->pNext = temp;
		list->pLast = temp;
	}
}

Node* getIndex(LList list, int i) {
	Node *run = list.pFirst;
	for (int x = 0; x < i; x++) {
		run = run->pNext;
	}
	return run;
}

void deleteFront(LList *list) {
	Node *toDelete = list->pFirst;
	list->pFirst = list->pFirst->pNext;

	delete toDelete;
	list->size--;
}

void deleteBack(LList *list) {
	Node *toDelete = list->pLast;
	list->pLast = getIndex(*list, list->size - 2);

	delete toDelete;
	list->size--;
}

void addAtIndex(LList *list, int value, int i) {
	if (list->size == 0) {
		addFront(list, value);
	}
	else {
		Node *target = getIndex(*list, i);
		Node *temp = new Node;
		temp->num = value;

		temp->pNext = target->pNext;
		target->pNext = temp;
	}
	list->size++;
}
void deleteAtIndex(LList *list, int i) {
	if (list->size == 1) {
		delete list->pFirst;
		list->pFirst = NULL;
		list->pLast = NULL;
	}
	else if (i == 0) {
		deleteFront(list);
	}
	else if (i == list->size - 1) {
		deleteBack(list);
	}
	else {
		Node *target = getIndex(*list, i);
		Node *trail = getIndex(*list, i - 1);
		trail->pNext = target->pNext;
		delete target;
	}
}

void reverseOrder(LList *list) {
	Node *tFirst, *tLast, *tRun;
	tFirst = tLast = list->pFirst;

	if (tFirst != NULL) {
		list->pFirst = list->pFirst->pNext;
		tRun = list->pFirst;
		do {
			list->pFirst = list->pFirst->pNext;
			tRun->pNext = tFirst;
			tFirst = tRun;
			tRun = list->pFirst;
		} while (list->pLast == NULL);
	}
}
void printList(LList list) {
	cout << "|";
	for (Node *pRun = list.pFirst; pRun != NULL; pRun = pRun->pNext) {
		cout << pRun->num << "|";
	}
}

void printReverse(LList *list) {
	reverseOrder(list);
	printList(*list);
	reverseOrder(list);
}



int main() {


	
	system("pause");
	return 0;
}