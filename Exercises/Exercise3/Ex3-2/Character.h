#pragma once

#include <string>
#include <vector>
using namespace std;

class Character{
	class Costume {
	private: 
		string name; //name of the costume
	};
	class Stats {//stats nested class
	private:
		int physAtck, physDef, magAtck, magDef, agi, lck;

	};
	enum Attribute {
		Null, Fire, Electric, Water, Light
	};
private:
	string name, title;
	int lvl, hpMax, hpCur, tpMax, tpCur, exp;
	Stats stats;
	Attribute attackAtt;
	vector<Attribute> weakAtt, resistAtt;


public:
	Character();
	~Character();

	void applyWeakAtt(Attribute);
		//this method adds an attribute that the character is weak to
	void applyAttackAtt(Attribute);
		//this method sets what attribute does the character's attack have
	void applyResistAtt(Attribute);
		//adds an attribute it can be resistant to
	bool isWeak(Attribute);
		//this checks if the character's is weak to the input attribute
	bool isResist(Attribute);
		//ditto, but the opposite

};

