#pragma once
#include<string>
#include<vector>

#include "Item.h"

using namespace std;

class Item;
class Clothes;
class Armor;
class Holster;
class Weapon;

class Player
{
private:
	string name;
	Clothes clothes[6];
	Armor armor;
	Holster holster;
	vector<Item> inventory;

public:
	Player();
	~Player();
	void equipItem(Item);
	void useWeapon(Weapon);
};

