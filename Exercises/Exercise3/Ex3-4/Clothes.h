#pragma once

class Image;
class Model;

class Clothes
{
protected:
	int type;
	Image img;
	Model model;


public:
	Clothes();
	~Clothes();

	void wear(Player);
	//upon calling this, player will wear this
};

