#pragma once
#include <string>

using namespace std;

class Prize
{
private:
	string name;
	int money;
public:
	Prize();
	Prize(string, int);
	~Prize();
	string getName();
	void setName(string);
	int getMoney();
	void setMoney(int);
	void printWin();

};

