#include <iostream>

#include "Prize.h"
#include "Player.h"
#include "WeightedRand.h"
#include "WeightedRand.cpp"

using namespace std;


//system pause: cross platform system("pause");
void sysPaws(string message = "", bool enter = false)
{
	//* 2 for windows, 1 for mac
	if (message.length() > 0) {
		cout << message << "...";
		system("pause>nul");
	}
	else
		system("pause");
	/*/
	string main = "read -n 1 -s -p \"";
	if ((message.length() == 0))
	message = "Press any key to continue";
	main += message;
	main += "...\"";
	system(main.c_str());
	//*/
	if (enter)
		cout << endl;
}
void sysCls()
{
	string msg;
	//*
	msg = "cls";
	/*/
	msg = "clear";
	//*/
	system(msg.c_str());
}

int filteredRangeInput(int min, int max, string msg) {
	bool error;
	int num;

	do {
		error = false;
		cout << msg<< endl;
		cin >> num;
		if (cin.fail()) {
			error = true;
			cout << "\tINPUT ERROR!!!" << endl;
			cin.ignore('\n', 10);
			cin.clear();
		}
		if (num < min || num > max) {
			error = true;
			cout << "\tERROR: input number from "<<min<<" to " << max << "!!!"<<endl;
		}
	} while (error);
	return num;
}

void sellInventoryUI(Player &play) {
	play.sellInventory(true);
}

void suggestSell(Player &play, bool ui = true) {

	bool yes = false;
	if (play.getInventoryMoney() > 0) {
		cout << "would you like to sell your inventory for" << endl;
		cout << "\t" << Player::commaInsert(play.getInventoryMoney()) << "?(1/0)";;
		cin >> yes;
		if (yes)
			play.sellInventory(ui);
	}
	else {
		play.printInventory();
		cout << "YOU'RE STILL BROKE!!!" << endl;
		sysPaws(" ",true);
	}
	
}

int getManualInput(int max) {
	return filteredRangeInput(0, max, "Number of times to Roll:(max is " + to_string(max) + ")");
}

void printRollTable(Player &play, WeightedRand<Prize*> &lottery) {
	static int x;
	play.printStats();
	cout << "=====Rolling Lottery=====" << endl;
	x = lottery.getItemRandIndex();
	play.addItem(x);
	play.printInventory();
}


void playGacha(Player &play, WeightedRand<Prize*> &lottery) {
	int cost = 25, opt = 0, times = 0;
	bool roll = true;
	vector<int> collate;

	for (int i = 0; i < lottery.getList()->size(); i++) {
		collate.push_back(0);
	}

	do{
		roll = true;
		times = play.getMoney() / cost;
		play.printStats();
		cout << "Ready to roll?";

		if (times >= 1) {
			char c;
			cout << endl;
			cout << "How many times to Roll:" << endl;
			cout << "\t[1] Once(cost: " << cost << ")" << endl;
			if (times >1){
				cout << "\t[2] Roll Max (you will Roll " << times << ")" << endl;
				cout << "\t[3] Roll Max-1 (you will Roll " << times - 1 << ")" << endl;
				cout << "\t[x] Manual Input (input from 1 to " << times << ")" << endl;
			}
			cout << "\t[0] Go back to Menu" << endl;

			cin >> c;
			cin.ignore('\n', 10);
			switch (c) {
			case '1': times = 1;  break;
			case '2': break;
			case '3': times--; break;
			case '0':times = 0; 
				roll = false; break;
			default: times = getManualInput(times);
				break;
			}
		}
		else {
			sysPaws(" ");
			cout << "NOT!!!... YOU'RE BROKE!!!" << endl;
			cout << "go sell some items" << endl;
			suggestSell(play);
			times = play.getMoney() / cost;
			roll = false;
		}

		if (times >= 1 && roll) {
			if (times < 10) {
				cout << endl << "=====Rolling Lottery=====" << endl;
				for (int i = 0; i < times && roll; i++) {
					int x = lottery.getItemRandIndex();
					lottery.getList()->at(x)->printWin();
					play.addMoney(-cost);
					collate[x]++;
				}

				play.addItems(collate);
				for (int i = 0; i < collate.size(); i++) {
					collate[i] = 0;
				}
			}
			else {
				for (int i = 0; i < times; i++) {
					sysCls();
					printRollTable(play, lottery);
					play.addMoney(-cost);
				}
			}
			cout << endl << "======Ending Lottery=====" << endl;
		}
		if(times <= 0)
			cout << "you will now return to the Main Menu";

		sysPaws(" ", true);
		sysCls();
	} while (times > 0);
	//ends here
}

int main() {
	int target = 10000;
	Player play(100,10000);
	WeightedRand<Prize*> lottery;
	int option = 0;
	//Hardcodes the contents in the 
	lottery.addItem(new Prize("Lucky Star",1000),1);
	lottery.addItem(new Prize("Bad Luck", 0), 6);
	lottery.addItem(new Prize("Fortune Cookie", 500), 5);
	lottery.addItem(new Prize("Good Fortune", 300), 10);
	lottery.addItem(new Prize("Rainbow Star", 100), 18);
	lottery.addItem(new Prize("Great Star", 50), 20);
	lottery.addItem(new Prize("So-so", 25), 100);
	lottery.addItem(new Prize("Poop", 10), 40);

	play.setInventory(lottery.getList());

	cout << "Welcome to random Lottery!!!" << endl;
	do {
		play.printStats();
		string msg = "Select Mode:\n";
		msg += "\t[1] Play game\n";
		msg += "\t[2] view inventory\n";
		msg += "\t[3] sell Items";
		option = filteredRangeInput(1, 3, msg);
		sysCls();
		switch (option) {
		case 1: 
			playGacha(play, lottery);
			break;
		case 2:
			play.printInventory();
			suggestSell(play,false);
			sysPaws(" ");
			break;
		case 3:
			play.sellInventory();
			sysPaws(" ");
			break;
		default: break;
		}
		sysCls();


	} while (play.getTotalMoney() > 0 && play.getTotalMoney() < target);

	play.printStats();
	if (play.getMoney() >= play.getTarget()) {
		cout << "YOU WIN!!!" << endl;
	}else{
		cout << "YOU BROKE!!!" << endl;
	}

	system("pause");
	return 0;

}