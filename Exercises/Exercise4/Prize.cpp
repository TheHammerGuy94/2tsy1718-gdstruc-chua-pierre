#include <iostream>
#include "Prize.h"


Prize::Prize(){
	name = "";
	money = 0;
}

Prize::Prize(string name, int money){
	this->name = name;
	this->money = money;
}

Prize::~Prize(){}

string Prize::getName()
{
	return this->name;
}

void Prize::setName(string name){
	this->name = name;
}

int Prize::getMoney(){
	return this->money;
}

void Prize::setMoney(int money){
	this->money = money;
}

void Prize::printWin(){
	cout << "You won " << name<<",\n\t"
		<< "worth " << money << endl;
}
