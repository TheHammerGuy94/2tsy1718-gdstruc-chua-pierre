#include "Player.h"



Player::Player(){
	this->money = 0;
	this->target = 0;
}

Player::Player(int money,int target){
	this->money = money;
	this->target = target;
}


Player::~Player(){}

int Player::getMoney()
{
	return this->money;
}

void Player::setMoney(int money){
	if(money >= 0)
		this->money = money;

}

void Player::addMoney(int money){
	this->money += money;
}

int Player::getTarget(){
	return this->target;
}

void Player::setTarget(int target){
	this->target = target;
}

void Player::setInventory(vector<Prize*> *inventory){
	this->inventory = inventory;
	this->collate.clear();
	for (auto i : *inventory)
		collate.push_back(0);
}

int Player::getTotalMoney(){
	return money + getInventoryMoney();
}

vector<Prize*>* Player::getInventory(){
	return this->inventory;
}

int Player::getInventoryMoney(){
	int total = 0;
	for (int i = 0; i < collate.size(); i++) {
		total += inventory->at(i)->getMoney() * collate[i];
	}
	return total;
}

void Player::addItem(int i){
	collate[i] ++;
	qty++;
}

void Player::addItems(vector<int> collate){
	for (int i = 0; i < collate.size(); i++) {
		this->collate[i] += collate[i];
		qty += collate[i];
	}
}

string Player::commaInsert(int num) {
	string word = to_string(num);
	int displace = 0, numSize = word.length(), i = 0;
	displace = word.length() % 3;

	for (i = displace; i < word.length(); i++) {
		if ((i - displace) % 3 == 0) {
			word.insert(i, ",");
			displace++;
			i++;
		}
	}
	if (word[0] == ',')
		word.erase(0, 1);
	return word;
}
void Player::sellInventory() {
	sellInventory(true);
}

void Player::sellInventory(bool ui){
	int total = 0;
	if(ui)
		printInventory();
	if (qty > 0) {
		for (int i = 0; i < collate.size(); i++) {
			total += inventory->at(i)->getMoney()*collate[i];
			collate[i] = 0;
		}
		cout << "you have sold:" << endl
			<< "\t" << commaInsert(total) << "  worth of Prizes" << endl;
		money += total;
	}
}

void Player::printStats(){
	cout << "money:" << commaInsert(money)<<"/"<<commaInsert(target)<<((money >= target)?" TARGET REACHED!!!":"")<<endl;
}

//YOU HAVE NO IDEA HOW MUCH TIME I PROGGRAMMED THIS!!!
void Player::printInventory(){
	//HOLY SONOFA WHAT DAFAQ IS THIS!??!?!

	if (qty <= 0)
		cout << "there are no items in the inventory" << endl;
	else {
		int qtyW, nameW, valueW, totalW;
		string qtyS = "qty", nameS = "prize name", valueS = "value", totalS = "total";
		qtyW = qtyS.length();
		nameW = nameS.length();
		valueW = valueS.length();
		totalW = totalS.length();

		//gets the maximum possible length of every column
		for (int i = 0; i<collate.size(); i++) {
			if (collate[i] > 0) {
				qtyW = max(qtyW, (int)to_string(collate[i]).length());
				nameW = max(nameW, (int)inventory->at(i)->getName().length());
				valueW = max(valueW, (int)to_string(inventory->at(i)->getMoney()).length());
				totalW = max(totalW, (int)to_string(inventory->at(i)->getMoney()*collate[i]).length());
			}
		}

		//printing column headers
		cout << "Your Inventtory contains:" << endl;
		cout<<left<<setw(qtyW)<< qtyS << "|" 
			<<left<<setw(nameW)<< nameS << "|" 
			<<left<<setw(valueW)<< valueS << "|" 
			<<left <<setw(totalW)<< totalS << "|" << endl;

		//printing header's row divider
		for (int i = 0; i < (qtyW + nameW + valueW + totalW + 4); i++)
			cout << "=";
		cout << endl;
		//printing contents
		for (int i = 0; i < collate.size(); i++) {
			if (collate[i] >0)
			{
				cout << right << setw(qtyW) << collate[i] << "|"
					<< left << setw(nameW) << inventory->at(i)->getName() << "|"
					<< right << setw(valueW) << inventory->at(i)->getMoney() << "|"
					<< right << setw(totalW) << inventory->at(i)->getMoney()*collate[i] << "|"  << endl;
			}
		}
	}
}

void Player::printTest(){
	//DO NOT USE THIS IN MAIN, EXCEPT FOR TESTING ONLY!!!
	collate[2] = 5;
	collate[3] = 10;
	collate[0] = 1;
	this->qty = 1;
	printInventory();
}
