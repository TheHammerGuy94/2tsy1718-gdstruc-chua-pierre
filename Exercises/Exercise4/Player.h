#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <algorithm>
#include "Prize.h"

using namespace std;

class Player
{
private:
	int money,target,qty = 0;
	vector<Prize*> *inventory = NULL;
	vector<int> collate;
public:
	Player();
	Player(int moeny, int target);
	~Player();
	static string commaInsert(int);
	int getMoney();
	void setMoney(int);
	void addMoney(int);
	int getTarget();
	void setTarget(int);
	void setInventory(vector<Prize*> *);
	int getTotalMoney();
	vector<Prize*>* getInventory();
	int getInventoryMoney();
	void addItem(int);
	void addItems(vector<int>);
	void sellInventory();
	void sellInventory(bool);
	void printStats();
	void printInventory();
	void printTest(); 
};

