
#include <random>
#include <ctime>
#include "WeightedRand.h"

template<class I>
WeightedRand<I>::WeightedRand() {}

template<class I>
WeightedRand<I>::~WeightedRand() {}

template<class I>
int WeightedRand<I>::getRand(int i) {
	static default_random_engine rd((unsigned)time(0));
	static uniform_real_distribution<float> randFl(0.0f, 1.0f);;
	return floor(i * randFl(rd));
}

/*this inserts a new Item into the weighted randomList
weight can be any value, just note the proportions of the weights
since the chance will be based on the summation of the weights
*/
template<class I>
void WeightedRand<I>::addItem(I i, int weight) {
	this->list.push_back(i);
	this->weights.push_back(weight);
	this->collate.push_back(0);
	totalWeight += weight;
}

template<class I>
vector<I>* WeightedRand<I>::getList() {
	return &this->list;
}

template<class I>
const vector<int>* WeightedRand<I>::getCollated(){
	return &this->collate;
}

template<class I>
void WeightedRand<I>::removeItem(int i) {
	list.erase(list.begin() + i);
	totalWeight -= weights[i];
	weights.erase(weights.begin() + i);
	collate.erase(collate.begin() + i);
}

template<class I>
int WeightedRand<I>::getTotalWeight() {
	return this->totalWeight;
}

//this will return you the index only. perfect for counting occurrences
template<class I>
int WeightedRand<I>::getItemRandIndex() {
	int chance = getRand(getTotalWeight());
	int found = -1;
	for (int i = 0; i < weights.size() && found == -1; i++) {
		if (chance < weights[i])
			found = i;
		else
			chance -= weights[i];
	}
	collate[found]++;
	return found;
}

//this will return you the actual object
template<class I>
I WeightedRand<I>::getItemRand() {
	return list[getItemRandIndex()];
}
