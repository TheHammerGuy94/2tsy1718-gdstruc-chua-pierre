#pragma once
#include <vector>
#include <cmath>
#include <string>

using namespace std;

template <class I>
class WeightedRand
{
private:
	vector<I> list;
	vector<int> weights,collate;
	int totalWeight = 0;
	static int getRand(int);
public:
	WeightedRand<I>();
	~WeightedRand<I>();

	void addItem(I i,int weight);
	vector<I>* getList();
	const vector<int>* getCollated();
	void removeItem(int i);
	int getTotalWeight();
	int getItemRandIndex();
	I getItemRand();
};
