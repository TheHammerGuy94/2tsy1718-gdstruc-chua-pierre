/*
	2 GDSTRUCT
	by Pierre Chua
*/
#include <iostream>
#include <string>
#include <map>
#include <random>
#include <ctime>

#include "Hero.h"

using namespace std;

/*
	note: 
		0 = space
		1 = wall
		9 = her0
		8 = goal
		2 = potion
		3 = blackHole
*/

enum Direction {
	up, down, lft, rght, nope
};

Direction charToDir(char c) {
	switch(toLower(c)){
		case 'w': return up;
		case 's': return down;
		case 'a': return lft;
		case 'd': return rght;
		default: return nope;
	}
}

bool heroMove(Hero &h,char c, int map[][30]){
	return heroMove(h, charToDir(c),map);
}
bool heroMove(Hero &h, Direction d, int map[][30]) {
	bool move = false;
	int x = h.getDim()->getX();
	int y = h.getDim()->getY();
	switch (d) {
		case up: 
			if(map[y - 1][x] != 1)
				h.getDim()->incY(-1); move = true; break;
		case down:
			if (map[y + 1][x] != 1)
				h.getDim()->incY(+1); move = true; break;
		case lft: 
			if (map[y][x - 1] != 1)
				h.getDim()->incX(-1); move = true; break;
		case rght: 
			if (map[y][x + 1] != 1)
				h.getDim()->incX(+1); move = true; break;
	}
	return move;
}

char uiPlayerMove() {
	char c = ' ';

	cout << "here would you like to go:" << endl;
	cout << "\t[W] up" << endl;
	cout << "\t[S] down" << endl;
	cout << "\t[A] left" << endl;
	cout << "\t[D] right" << endl;
	cout << "-->>";
	cin >> c;
	cin.clear();
	cin.ignore('\n', 10);
	return c;
}

int randInt(int i) {
	return rand() % i;
}

void initItemsZero(int itemMap[][30]) {
	for (int i = 0; i < 15; i++) {
		for (int j = 0; j < 30; j++) {
			itemMap[i][j] = 0;
		}
	}
}


void initItems(int itemMap[][30], int wallMap[][30]) {
	int x, y;

	//places potions
	for (int i = 0; i < 5; i++) {
		do {
			x = randInt(15);
			y = randInt(30);
		} while (wallMap[x][y] != 0 && itemMap[x][y] !=0);
		itemMap[x][y] = 2;
	}
	//places black Holes
	for (int i = 0; i < 2; i++) {
		do {
			x = randInt(15);
			y = randInt(30);
		} while (wallMap[x][y] != 0 && itemMap[x][y] != 0);
		itemMap[x][y] = 3;
	}
}

void printMap(int map[][30],Hero player) {
	char c = '\0';
	for (int i = 0; i < 15; i++) {
		for (int j = 0; j < 30; j++) {
			if (player.getDim()->getX() == j &&
				player.getDim()->getY() == i) {
				c = 'H';
			} else {
				switch (map[i][j]) {
					case 1: c = 'X'; break;
					case 8: c = 'G'; break;
					default: c = ' '; break;
				}
			}
			cout << c;
		}
		cout << endl;
	}
}


void insertPotion(Hero &play) {
	play.addItem("potion");
	
}
void ohNo(Hero &play) {
	play.resetPosition();
	cout << "OH NO A BLACK HOLE!!!" << endl;
}

bool checkTile(int itemMap[][30],int map[][30], Hero &play) {
	int x = play.getDim()->getX();
	int y = play.getDim()->getY();
	bool reset = false;

	if(map[y][x] == 8){
		cout << "GOAL" << endl;
		reset = true;
	}
	else {
		switch (itemMap[y][x]) {
			case 2:insertPotion(play);
				break;
			case 3:ohNo(play);
				reset = true;
				break;
		}
	}
	return reset;

}
void resetMap(int itemMap[][30], int map[][30]) {
	initItemsZero(itemMap);
	initItems(itemMap, map);
}

int main() {
	Hero player;
	bool play= false;
	bool moved = false;
	bool reset = false;
	char c = ' ';
	Direction d = nope;

	srand(unsigned(time(0)));

	int map[15][30] =
	{
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
		{ 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
		{ 1, 8, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
	};
	int itemPlace[15][30];

	resetMap(itemPlace, map);

	printMap(map,player);

	play = true;
	while (play) {
		if (reset) {
			resetMap(itemPlace, map);
			reset = false;
		}
		d= charToDir(uiPlayerMove());
		system("cls");
		moved = heroMove(player, d, map);
		printMap(map,player);

		reset = checkTile(itemPlace, map, player);
	}


	cout << "...";
	system("pause>null");
	return 0;
}