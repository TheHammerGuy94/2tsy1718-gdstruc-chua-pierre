#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "Dim.h"

using namespace std;

class Hero
{
private:
	string name;
	vector<string> items;
	Dim pos;
public:
	Hero();
	~Hero();

	string getName();
	void setName(string);
	void resetPosition();

	void addItem(string);
	Dim* getDim();




};

