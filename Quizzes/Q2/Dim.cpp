#include "Dim.h"



Dim::Dim(){
	x = y = 0;
}

Dim::Dim(int x, int y){
	this-> x= x;
	this-> y = y;
}

Dim::~Dim(){}

int Dim::getX(){
	return this-> x;
}

void Dim::setX(int x){
	this->x = x;
}

int Dim::getY(){
	return this-> y;
}

void Dim::setY(int y){
	this->y = y ;
}

void Dim::incX(int x)
{
	this->x += x;
}

void Dim::incY(int y)
{
	this->y += y;
}
