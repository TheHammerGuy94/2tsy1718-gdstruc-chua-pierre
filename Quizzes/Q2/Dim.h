#pragma once
class Dim
{
private:
	int x, y;
public:
	Dim();
	Dim(int x, int y);
	~Dim();
	int getX();
	void setX(int);
	int getY();
	void setY(int);
	void incX(int);
	void incY(int);
};

