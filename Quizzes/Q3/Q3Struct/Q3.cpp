#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
using namespace std;

struct Node{
	int num;
	Node* next = NULL;
};
struct LList{
	unsigned int size = 0;
	Node *head = NULL, *tail= NULL;
};

template<class I>
void swap(I *i, I *j) {
	I temp = *i;
	*i = *j;
	*j = temp;
}

int randMinMax(int min, int max) {
	static default_random_engine rd((unsigned)time(0));
	static uniform_real_distribution<double> randFloat(0.0f, 1.0f);
	if (max < min) swap(&min, &max);

	if (min == max)
		return min;
	else {
		return (int)floor(randFloat(rd)*(max + 1 - min) + min);
	}
}

Node* createNode(){
	Node *newNode = new Node;
	bool error = false;
	int i;
	do{
		error = false;
		cout<<"input number for new Node>>";
		cin >> i;
		if(cin.fail()){
			cout<<"input error"<<endl;
			cin.ignore('\n', 10);
			cin.clear();
		}
	}while(error);
	newNode->num = i;
	cout << "Node created with Number " << i << endl;
	return newNode;
}
Node* getNodeAt(LList list, int i) {
	Node *run = list.head;
	for (int x = 0; x < i; x++) {
		run = run->next;
	}
	return run;
}
//1 - printList
void printList(LList list){
	cout<<"your list contains:"<<endl;
	if(list.size <= 0)
		cout<<"Nothing"<<endl;
	else{
		for (Node *run = list.head; run != NULL; run = run->next) {
			cout<<run->num<<"->";
		}
		cout << "NULL";
	}
}
//2 - push
void pushNode(LList *list, Node *node){
	cout << "pushing new node to front..." << endl;
	node->next = list->head;
	list->head = node;
	list->size++;
}
void pushNode(LList *list){
	Node *newNode = createNode();
	pushNode(list, newNode);
}
//3 - pop

void deleteBack(LList *list);
void popNode(LList*list){
	cout << "Deleting Node in the back" <<endl;
	deleteBack(list);
}
//4 - insert
void addAtIndex(LList *list, int i) {
	Node *newNode = createNode();
	cout << "insertin node at index: " << i << endl;
	if (list->size == 0) {
		pushNode(list, newNode);
	}
	else {
		Node *target = getNodeAt(*list, i);

		newNode->next = target->next;
		target->next = newNode;

		list->size++;
	}
}
void deleteBack(LList *list) {
	Node *toDelete = list->tail;
	list->tail = getNodeAt(*list, list->size - 2);

	delete toDelete;
	list->size--;
}
//5 - delete
void deleteAtIndex(LList *list, int i) {
	if (list->size == 1) {
		delete list->head;
		list->head = NULL;
		list->tail = NULL;
	}
	else if (i == 0) {
		popNode(list);
	}
	else if (i == list->size - 1) {
		deleteBack(list);
	}
	else {
		Node *target = getNodeAt(*list, i);
		Node *trail = getNodeAt(*list, i - 1);
		trail->next = target->next;
		delete target;
	}
}
//6 - swap
void swap(Node *n1, Node *n2){
	swap(&n1->num, &n2->num);
}
void swap(LList *list, int i, int j){
	Node *n1, *n2;
	n1 = getNodeAt(*list,i);
	n2 = getNodeAt(*list,j);
	swap(n1,n2);
}
//7 - shuffle
void shuffle(LList *list){
	Node *n1, *n2;
	cout << "Shufling List..." << endl;
	for(int i = 0; i< (int)list->size; i++){
		n1 = getNodeAt(*list, i);
		n2 = getNodeAt(*list,randMinMax(i, list->size-1));
		swap(n1,n2);
	}
	cout << "List Shuffled..." << endl;
}
//8 - selection sort
void selectionSort(LList *list){
	cout << "sorting via Selection Sort..." <<endl;
	for(Node *n1= list->head; n1 != NULL; n1 = n1->next){
		Node *sm = n1;
		for(Node *n2 = n1; n2 != NULL; n2 = n2->next){
			if(n2->num < sm->num)
				sm = n2;
		}
		swap(n1, sm);
	}
	cout << "sorting complete!!!" << endl;
}
//bubbleSort
void bubbleSort(LList *list){
	Node *last;
	cout << "sorting via Bubble Sort..." << endl;
	for(int i = list->size -1; i >0; i--){
		last = getNodeAt(*list,i);
		for (Node *n2 = list->head; n2 != last; n2 = n2->next) {
			if (n2->num > n2->next->num) swap(n2, n2->next);
		}

	}

	cout << "sorting complete!!!" << endl;
}

void swapUI(LList *list) {
	int i, j;

	cout << "input first index (from 0 to "<<list->size-1<<")>>";
	cin >> i; 
	cout << "input second index (from 0 to " << list->size - 1 << ")>>";
	cin >> j;
	cout << "swapping " << i << " & " << j << "..." << endl;
	swap(list, i, j);
}
void deleteEverything(LList *list) {
	while (list->head != NULL) {
		popNode(list);
	}
}

int main() {
	LList list;
	int select = 0;
	int input = 0;

	do{
		printList(list);
		cout<<endl;
		cout<<"Select Action:"<<endl;
		cout<<"\t[1]- push"<<endl;
		cout<<"\t[2]- pop"<<endl;
		cout<<"\t[3]- insert"<<endl;
		cout<<"\t[4]- delete"<<endl;
		cout<<"\t[5]- swap"<<endl;
		cout<<"\t[6]- shuffle"<<endl;
		cout<<"\t[7]- selection sort"<<endl;
		cout<<"\t[8]- bubble sort"<<endl;
		cout<<">>";
	
		cin >> select;
	
		cout << "====================" << endl;
	
		switch(select){
			case 1: pushNode(&list);
					break;
			case 2: //	3 - pop
				popNode(&list);
				break;
			case 3: //	4 - insert
				cout << "Add new Node from 0 to " << list.size - 1<<endl;
				cin >> input;
				addAtIndex(&list, input);
				break;
			case 4: //	5 - delete
				cout << "delete a node from 0 to " << list.size - 1 << endl;
				cin >> input;
				deleteAtIndex(&list, input); break;
			case 5: //	6 - swap
				swapUI(&list);break;
			case 6: //	7 - shuffle
				shuffle(&list); break;
			case 7: //	8 - selection sort
				selectionSort(&list); break;
			case 8: //	9 - bubble sort
				bubbleSort(&list);break;
			default:
				cout << "Terminating Program!!!" << endl;
				break;
		}
		system("pause");
		system("cls");
	}while(select >= 1 && select <= 8);

	deleteEverything(&list);
	return 0;
}