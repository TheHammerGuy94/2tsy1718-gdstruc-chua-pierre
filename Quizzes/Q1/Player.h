#pragma once

#include <iostream>
#include "Stats.h"
#include "Weapon.h"

using namespace std;

class Player{
public:
    enum Job{
        Mage = 1, Warrior = 2, Rouge = 3,Null = 0
    };
    
private:
    string name;
    Stats stats;
    Job job;
    Weapon weapon;
    int level, exp;
    int hpMax, mpMax;
    
public:
    Player();
    ~Player();
    string getName();
    void setName(string);
    Stats* getStats();
    void setWeapon(Weapon);
    Weapon getWeapon();
    Job getJob();
    void setJob(Job);
    string jobToString();
    int getLevel();
    void setLevel(int);
    int getExp();
    void setExp(int);
    int getHpMax();
    int getMPMax();
    
    void printPlayer();
    
    static string jobToString(Job);
    static Job intToJob(int);
    
    
    
    
    
    
    
    
};
