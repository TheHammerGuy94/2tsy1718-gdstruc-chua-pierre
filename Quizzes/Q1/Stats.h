#pragma once
#include <stdio.h>
using namespace std;

class Player;

class Stats{
private:
    Player *play;
    int stats[5];
    
public:
    const int STA = 0, STR = 1, CON = 2, ATK = 3, DEF = 4;
    
    Stats();
    Stats(int str, int sta, int con);
    ~Stats();
    
    int getStats(int i);
    void setPlayer(Player*);
    void setSTR(int i);
    void setSTA(int i);
    void setCON(int i);
    
    int calcHP();
    int calcMP();
    void calcATK();
    void calcDEF();
    
    void printStats();
    

};
