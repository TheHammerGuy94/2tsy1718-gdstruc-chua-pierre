#include "Weapon.h"

Weapon::Weapon(){
	setName("");
	setDesc("");
	setLevel(0);
	setAtck(0);
}
Weapon::Weapon(string name, string desc, int level, int atck){
	setName(name);
	setDesc(desc);
	setLevel(level);
	setAtck(atck);
}
Weapon::~Weapon(){}
string Weapon::getName(){
	return this->name;
}
string Weapon::getDesc(){
	return this->desc;
}
int Weapon::getLevel(){
	return this-> level;
}
int Weapon::getAtck(){
	return this-> atck;
}
void Weapon::setName(string name){
	this->name = name;
}
void Weapon::setDesc(string desc){
	this->desc = desc;
}
void Weapon::setLevel(int level){
	this->level = level;
}
void Weapon::setAtck(int atck){
	this-> atck= atck;
}
