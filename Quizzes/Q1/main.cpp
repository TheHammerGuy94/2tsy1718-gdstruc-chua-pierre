/*
 Quiz 1 GDStruct
 Made by Pierre Chua
 */
#include <iostream>
#include "Player.h"
#include "Weapon.h"



using namespace std;

void sysPaws(string message = "", bool enter = false){
    /* 2 for windows, 1 for mac
    if (message.length() > 0) {
        cout << message << "...";
        _getch();
    }
    else
    {
        system("pause");
    }
    /*/
     string main = "read -n 1 -s -p \"";
     if ((message.length() == 0)) {
         message = "Press any key to continue";
     }
     main += message;
     main += "...\"";
     system(main.c_str());
     //*/
    if (enter)
        cout << endl;
}
void sysCls(){
    string msg;
    /*
    msg = "cls";
    /*/
     msg = "clear";
     //*/
    system(msg.c_str());
}
void setName(Player &play){
    string name;
    cout << "What is your name? >>";
    cin >> name;
    
    play.setName(name);
}
void setJob(Player &play){
    int job = 0;
    bool error = false;
    
    do{
        error = false;
        cout << "What is your job?"<<endl;
        cout << "1 - mage, 2 - Warrior, 3 - Rouge"<<endl;
        cout <<"input number >>";
        cin >>job;
        
        if(job <1 || job >3){
            cin.clear();
            cin.ignore('\n',100);
            cout <<"wrong input!!!"<<endl;
            error = true;
        }
        
    }while (error);
    play.setJob(Player::intToJob(job));
}

void setStats(Player &play){
    int temp;
    bool error = false;
    
    do{
        error = false;
        cout << "input strength stat>> ";
        cin >> temp;
        
        if(temp <0){
            cin.clear();
            cin.ignore('\n',100);
            cout <<"input cannot be 0"<<endl;
            error = true;
        }
        
        
    }while(error);
    play.getStats()->setSTR(temp);
    
    do{
        error = false;
        cout << "input stamina stat>> ";
        cin >> temp;
        if(temp <0){
            cin.clear();
            cin.ignore('\n',100);
            cout <<"input cannot be 0"<<endl;
            error = true;
        }

    }while(error);
    play.getStats()->setSTA(temp);
    
    do{
        error = false;
        cout << "input concentration stat>> ";
        cin >> temp;
        if(temp <0){
            cin.clear();
            cin.ignore('\n',100);
            cout <<"input cannot be 0"<<endl;
            error = true;
        }
    }while(error);
    play.getStats()->setCON(temp);
}

void printPlayer(Player &play){
    play.printPlayer();
}


int main() {
    Player player;
    Weapon weapons[3] = {Weapon("Dagger","Dagger Blade",1,100),
                        Weapon("Staff", "Staff Light",10, 200),
                        Weapon("Sword","Great Sword",20,300)};
    
    sysPaws();
    return 0;
}





