#include "Player.h"

using namespace std;

Player::Player(){
    /*
     string name;
     Stats stats;
     Job job;
     Weapon weapon;
     int level, exp;
     int hpMax, mpMax;
     */
    setName("");
    setJob(Null);
    level = 0;
    exp = 0;
    hpMax = 0;
    mpMax = 0;
}
Player::~Player(){

}
string Player::getName(){
	return this->name ;
}
void Player::setName(string name){
		this->name = name;
}
Stats* Player::getStats(){
	return &(this->stats) ;
}
void Player::setWeapon(Weapon weapon){
	this->weapon = weapon;
}
Weapon Player::getWeapon(){
	return this-> weapon;
}
Player::Job Player::getJob(){
	return this->job;
}
void Player::setJob(Player::Job job){
	this->job = job;
}
string Player::jobToString(){
    return Player::jobToString(this->job);
}
int Player::getLevel(){
	return this->level ;
}
void Player::setLevel(int lvl){
    this->level = lvl;
}
int Player::getExp(){
	return this->exp;
}
void Player::setExp(int exp){
	this->exp = exp;
}
int Player::getHpMax(){
	return this->hpMax;
}
int Player::getMPMax(){
	return this->mpMax;
}

void Player::printPlayer(){
    
    cout<< "name:\t"<<this->getName()<<endl;
    cout<< "Job:\t"<<this->jobToString()<<endl;
    cout<<"Weapon:"<<this->getWeapon().getName()<<endl;
    cout<<"Level: "<<this->getLevel()<<endl;
    cout<<"Exp:\t"<<this->getExp()<<endl;
    cout<<"HP:\t"<<this->getHpMax()<<"/"<<this->getHpMax()<<endl;
    cout<<"MP:\t"<<this->getMPMax()<<"/"<<this->getMPMax()<<endl;
    cout<<"=========="<<endl;
    this->stats.printStats();

}

string Player::jobToString(Job job){
	switch(job){
        case Mage: return "Mage";
		case Warrior: return "Warrior";
		case Rouge: return "Rouge";
		default: return "What";
	}
}
Player::Job Player::intToJob(int job){
	switch(job){
		case 1: return Mage;
		case 2: return Warrior;
		case 3: return Rouge;
		default: return Null;
	}
}





