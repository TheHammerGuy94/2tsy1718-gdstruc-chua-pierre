#pragma once

#include <string>

using namespace std;

class Weapon {
private:
    string name, desc;
    int level, atck;
public:
    Weapon();
    Weapon(string name, string desc, int level, int atck);
    ~Weapon();
    
    string getName();
    string getDesc();
    int getLevel();
    int getAtck();
    void setName(string);
    void setDesc(string);
    void setLevel(int);
    void setAtck(int);
    
    
};
