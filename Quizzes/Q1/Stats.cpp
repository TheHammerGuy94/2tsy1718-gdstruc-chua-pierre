#include <iostream>
#include <cmath>
#include "Stats.h"
#include "Player.h"

using namespace std;

Stats::Stats(){
    setSTR(0);
    setSTA(0);
    setCON(0);
}
Stats::Stats(int str, int sta, int con){
    setSTR(str);
    setSTA(sta);
    setCON(con);
}
Stats::~Stats(){}

int Stats::getStats(int i){
    return this->stats[i];
}

void Stats::setPlayer(Player *play){
    this->play = play;
}

void Stats::setSTR(int i){
    this->stats[STR] = i;
}
void Stats::setSTA(int i){
    this->stats[STA] = i;
}
void Stats::setCON(int i){
    this->stats[CON] = i;
}
int Stats::calcHP(){
    return 100 * this->play->getLevel() + 5*stats[STA];
}
int Stats::calcMP(){
    //Max MP is 10 per Level and additional 3.5 per CON
    return 10*  this->play->getLevel()  + round(3.5f * stats[CON]);
}
void Stats::calcATK(){
    //ATK is 5 per Level, additional 2 per STR and Weapon Damage
    stats[ATK] = 5 * this->play->getLevel() + 2 * stats[STR] + this->play->getWeapon().getAtck();
}
void Stats::calcDEF(){
    //DEF is 2 per Level and additional 0.6 per STA
    stats[DEF] = 2 * this->play->getLevel() + round(stats[DEF] * 0.6f);
}
void Stats::printStats(){
    //    const int STA = 0, STR = 1, CON = 2, ATK = 3, DEF = 4;
    cout<<"Stamina: "<<stats[0]<<endl;
    cout<<"Strength: "<<stats[1]<<endl;
    cout<<"Concentrate: "<<stats[2]<<endl;
    cout<<"Attack: "<<stats[3]<<endl;
    cout<<"Defense: "<<stats[4]<<endl;
}
